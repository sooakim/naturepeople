package kr.edcan.lumihana.naturepeople;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class ImageActivity extends AppCompatActivity {
    ImageView bigView, backButton, share;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        final Intent intent = getIntent();
        bigView = (ImageView) findViewById(R.id.bigView);
        backButton = (ImageView) findViewById(R.id.backButton);
        share = (ImageView) findViewById(R.id.share);

        bigView.setImageBitmap(BitmapFactory.decodeFile(intent.getStringExtra("image")));

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(Intent.ACTION_SEND)
                        .setType("image/*")
                        .putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///" + intent.getStringExtra("image")));
                startActivity(intent2);
            }

        });
    }
}

