package kr.edcan.lumihana.naturepeople;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;

public class MyIntro extends AppIntro {
    @Override
    public void init(Bundle savedInstanceState) {
        // Add your slide's fragments here.
        // AppIntro will automatically generate the dots indicator and buttons.
        addSlide(Slide.newInstance(R.layout.tuto_slide1));
        addSlide(Slide.newInstance(R.layout.tuto_slide2));
        addSlide(Slide.newInstance(R.layout.tuto_slide3));

        setDepthAnimation();
    }

    @Override
    public void onSkipPressed() {
        setOk();
    }

    @Override
    public void onNextPressed() {

    }

    @Override
    public void onDonePressed() {
        setOk();
    }

    @Override
    public void onSlideChanged() {

    }

    private void setOk(){
        SharedPreferences sharedPreferences = getSharedPreferences("play", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("isPlayed", true);
        editor.commit();
        finish();
    }
}