package kr.edcan.lumihana.naturepeople;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by kimok_000 on 2016-03-27.
 */
public class GalleryAdapter extends ArrayAdapter<String> {
    private LayoutInflater mInflater;

    public GalleryAdapter(Context context, ArrayList<String> object) {
        super(context, 0, object);
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        View view = null;
        if (v == null) {
            view = mInflater.inflate(R.layout.gridview_content, null);
        } else {
            view = v;
        }
        final String data = this.getItem(position);
        if (data != null) {
            ImageView imageView = (ImageView) view.findViewById(R.id.image);
            imageView.setImageBitmap(BitmapFactory.decodeFile(data));
        }
        return view;
    }
}
