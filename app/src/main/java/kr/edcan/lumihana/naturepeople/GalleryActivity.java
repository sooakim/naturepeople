package kr.edcan.lumihana.naturepeople;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

public class GalleryActivity extends AppCompatActivity {
    ArrayList<String> arrayList = new ArrayList<>();
    GalleryAdapter adapter;
    GridView gridView;
    ProgressDialog mProgressDialog;
    String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/NaturePeople/complete";
    ImageView back, sync;
    RelativeLayout place;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        init();
    }

    private void init() {
        gridView = (GridView) findViewById(R.id.gridview);
        place = (RelativeLayout) findViewById(R.id.place);
        back = (ImageView) findViewById(R.id.backButton);
        sync = (ImageView) findViewById(R.id.reset);

        load();

        adapter = new GalleryAdapter(getApplicationContext(), arrayList);
        gridView.setAdapter(adapter);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), ImageActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("image", arrayList.get(position).toString());
                startActivity(intent);
            }
        });

    }

    private void load() {
        mProgressDialog = ProgressDialog.show(GalleryActivity.this, "",
                "잠시만 기다려 주세요.", true);

        File list = new File(path);
        String[] imgList = list.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                Boolean bOK = false;
                if (filename.toLowerCase().endsWith(".jpg")) bOK = true;
                return bOK;
            }
        });

        if (imgList == null) {
            place.setBackgroundResource(R.drawable.placeholder);
        } else {
            place.setBackground(null);
            for (int i = 0; i < imgList.length; i++) {
                arrayList.add(new String(path + "/" + imgList[i]));
            }
        }

        mProgressDialog.dismiss();
    }

    private void reset() {
        arrayList = new ArrayList<>();
        load();
        adapter = new GalleryAdapter(getApplicationContext(), arrayList);
        gridView.setAdapter(adapter);
    }
}
