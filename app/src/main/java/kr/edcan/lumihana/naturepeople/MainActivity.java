package kr.edcan.lumihana.naturepeople;

import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    ImageView mShutter, mGallery, mTimer;
    CameraSurfaceBack mSurface;
    String mRootPath;
    static final String PICFOLDER = "NaturePeople";
    int facing = Camera.CameraInfo.CAMERA_FACING_BACK;
    int cameraShot = 0;
    String[] cameraPhoto = new String[2];
    int timerTime = 0;
    CountDownTimer mCountDownTimer;
    TextView timerText;
    boolean pushed = false;
    Boolean isFirst;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = getSharedPreferences("play", 0);
        if(!sharedPreferences.getBoolean("isPlayed", false)){
            Intent i = new Intent(MainActivity.this, MyIntro.class);
            startActivity(i);
        }

        timerText = (TextView) findViewById(R.id.timerText);
        mSurface = (CameraSurfaceBack) findViewById(R.id.previewFrame);
        mShutter = (ImageView) findViewById(R.id.captureButton);
        mTimer = (ImageView) findViewById(R.id.timer);
        mGallery = (ImageView) findViewById(R.id.gallery);

        timerText.setVisibility(View.INVISIBLE);

        mTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pushed == true) {
                    timerTime = 0;
                    timerText.setText(timerTime + "");
                    timerText.setVisibility(View.INVISIBLE);
                    pushed = false;
                } else {
                    timerText.setVisibility(View.VISIBLE);
                    timerTime += 1;
                    timerText.setText(timerTime + "");
                }
            }
        });

        mTimer.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                pushed = true;
                if (timerTime != 0) {
                    timerTime = 0;
                    timerText.setVisibility(View.INVISIBLE);
                }
                return false;
            }
        });

        mGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), GalleryActivity.class));
            }
        });

        final Camera.PictureCallback mPicture = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                Calendar calendar = Calendar.getInstance();
                String FileName = String.format("SH%02d%02d%02d-%02d%02d%02d.jpg",
                        calendar.get(Calendar.YEAR) % 100,
                        calendar.get(Calendar.MONTH) + 1,
                        calendar.get(Calendar.DAY_OF_MONTH),
                        calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE),
                        calendar.get(Calendar.SECOND));
                String path = mRootPath + "/temp/" + FileName;

                File file = new File(path);
                try {
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(data);
                    fos.flush();
                    fos.close();
                } catch (Exception e) {
                    return;
                }

                Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri uri = Uri.parse("file://" + path);
                intent.setData(uri);
                sendBroadcast(intent);

                Toast.makeText(getApplicationContext(), "사진이 저장 되었습니다", Toast.LENGTH_SHORT).show();

                cameraPhoto[cameraShot - 1] = path;

                if (facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    if (cameraShot >= 2) {
                        cameraShot = 0;
                        pushed = false;
                        timerTime = 0;
                        cameraShot = 0;
                        mShutter.setImageResource(R.drawable.main_shutter);
                        startActivity(new Intent(getApplicationContext(), ResultActitivy.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK).putExtra("BackPhoto", cameraPhoto[0]).putExtra("FrontPhoto", cameraPhoto[1]));
                    }
                    camera.startPreview();
                } else {
                    timerTime = 0;
                    pushed = false;
                    timerText.setVisibility(View.INVISIBLE);
                    timerText.setText(timerTime + "");
                    mShutter.setImageResource(R.drawable.main_shutter_people);
//                    camera.stopPreview();
                    mSurface.mCamera.stopPreview();
//                    camera.release();
                    mSurface.mCamera.release();
//                    camera = null;
                    mSurface.mCamera = null;

//                    camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
                    mSurface.mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
//                    camera.setDisplayOrientation(90);
                    mSurface.mCamera.setDisplayOrientation(90);
                    try {
//                        camera.setPreviewDisplay(mSurface.mHolder);
                        mSurface.mCamera.setPreviewDisplay(mSurface.mHolder);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    camera.startPreview();
                    mSurface.mCamera.startPreview();
                    facing = Camera.CameraInfo.CAMERA_FACING_FRONT;
                }
            }
        };

        final Camera.AutoFocusCallback mAutoFocus = new Camera.AutoFocusCallback() {
            @Override
            public void onAutoFocus(boolean success, Camera camera) {
                mShutter.setEnabled(success);
                mSurface.mCamera.takePicture(null, null, mPicture);
            }
        };

        mShutter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timerTime != 0) {
                    mCountDownTimer = new CountDownTimer(timerTime * 1000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            // TODO Auto-generated method stub
                            mTimer.setEnabled(false);
                            mShutter.setEnabled(false);
                            mGallery.setEnabled(false);
                            timerText.setText(--timerTime + "");
                        }

                        @Override
                        public void onFinish() {
                            // TODO Auto-generated method stub
                            ++cameraShot;
                            mSurface.mCamera.autoFocus(mAutoFocus);
                            mTimer.setEnabled(true);
                            mShutter.setEnabled(true);
                            mGallery.setEnabled(true);
                            timerTime = 0;
                            timerText.setText(timerTime + "");
                            timerText.setVisibility(View.INVISIBLE);
                            mCountDownTimer = null;
                        }
                    }.start();
                } else {
                    ++cameraShot;
                    mSurface.mCamera.autoFocus(mAutoFocus);
                }
            }
        });

        mRootPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + PICFOLDER;
        File fileRoot = new File(mRootPath + "/temp");
        if (fileRoot.exists() == false) {
            if (fileRoot.mkdirs() == false) {
                Toast.makeText(getApplicationContext(), "사진을 저장할 폴더가 없습니다.", Toast.LENGTH_SHORT).show();
                finish();
                return;
            }
        }
    }
}
