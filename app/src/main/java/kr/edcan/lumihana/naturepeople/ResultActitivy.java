package kr.edcan.lumihana.naturepeople;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class ResultActitivy extends AppCompatActivity {
    Bitmap backPhoto, frontPhoto;
    ImageView back, front, backButton, addText, saveButton;
    TextView onText;
    RelativeLayout layout;
    Bitmap bitmap;
    String mRootPath;
    String PICFOLDER = "NaturePeople";
    //    Slider sl_discrete;
    SeekBar slider;

    ImageView share, change;
    String path;
    boolean changed = false;
    int alpha = 80;

    String textValue = null, sizeValue = null, colorValue = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_actitivy);

//        sl_discrete = (Slider) findViewById(R.id.slider_sl_discrete);
//        sl_discrete.setValue(80, true);
//        sl_discrete.setOnPositionChangeListener(new Slider.OnPositionChangeListener() {
//            @Override
//            public void onPositionChanged(Slider view, boolean fromUser, float oldPos, float newPos, int oldValue, int newValue) {
//                alpha = newValue;
//                changeImage();
//            }
//        });

        slider = (SeekBar) findViewById(R.id.slider);
        slider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                alpha = progress;
                changeImage();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        back = (ImageView) findViewById(R.id.back);
        front = (ImageView) findViewById(R.id.front);
        layout = (RelativeLayout) findViewById(R.id.layout);
        share = (ImageView) findViewById(R.id.share);
        change = (ImageView) findViewById(R.id.change);
        backButton = (ImageView) findViewById(R.id.backButton);
        addText = (ImageView) findViewById(R.id.addText);
        onText = (TextView) findViewById(R.id.onText);
        saveButton = (ImageView) findViewById(R.id.saveButton);
        share.setEnabled(false);

        Intent intent = getIntent();

        backPhoto = BitmapFactory.decodeFile(intent.getStringExtra("BackPhoto"));
        frontPhoto = BitmapFactory.decodeFile(intent.getStringExtra("FrontPhoto"));

        changeImage();

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveImage();
            }
        });

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (changed) {
                    changed = false;
                    changeImage();
                } else {
                    changed = true;
                    changeImage();
                }
            }
        });

        addText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(ResultActitivy.this);
                alert.setTitle("텍스트 추가");
                alert.setMessage("텍스트를 추가합니다.");
                alert.setIcon(R.mipmap.ic_launcher);

                LinearLayout layout = new LinearLayout(ResultActitivy.this);
                layout.setOrientation(LinearLayout.VERTICAL);

                LinearLayout.LayoutParams params =
                        new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT);

                layout.setLayoutParams(params);
                layout.setPadding(64, 0, 64, 0);

                final EditText text = new EditText(ResultActitivy.this);
                text.setInputType(InputType.TYPE_CLASS_TEXT);
                text.setHint("표시할 텍스트");
                text.setLayoutParams(params);
                layout.addView(text);

                final EditText size = new EditText(ResultActitivy.this);
                size.setInputType(InputType.TYPE_CLASS_NUMBER);
                size.setHint("글자 크기");
                size.setLayoutParams(params);
                layout.addView(size);

                final EditText color = new EditText(ResultActitivy.this);
                color.setInputType(InputType.TYPE_CLASS_TEXT);
                color.setHint("글자 색상(#제외 색상코드)");
                color.setLayoutParams(params);
                layout.addView(color);

                alert.setView(layout);
                alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        onText.setVisibility(View.VISIBLE);
                        textValue = text.getText().toString();
                        sizeValue = size.getText().toString();
                        colorValue = color.getText().toString();

                        if (!textValue.equals("")) onText.setText(textValue + "");
                        if (!sizeValue.equals("")) onText.setTextSize(Float.parseFloat(sizeValue));
                        if (!colorValue.equals(""))
                            onText.setTextColor(Color.parseColor("#" + colorValue));
                    }
                });

                alert.setNegativeButton("취소",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                onText.setVisibility(View.INVISIBLE);
                            }
                        });
                alert.show();
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveImage();
                Intent intent = new Intent(Intent.ACTION_SEND)
                        .setType("image/*")
                        .putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///" + path));
                startActivity(intent);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    private void changeImage() {
        change.setEnabled(false);
        if (changed == false) {
            front.setImageAlpha(alpha);
            front.setRotation(-90);
            back.setRotation(-270);
            back.setImageBitmap(backPhoto);
            front.setImageBitmap(frontPhoto);
        } else {
            front.setImageAlpha(alpha);
            front.setRotation(-270);
            back.setRotation(-90);
            back.setImageBitmap(frontPhoto);
            front.setImageBitmap(backPhoto);
        }
        change.setEnabled(true);
    }

    private void saveImage() {
        change.setEnabled(false);
        addText.setEnabled(false);
        share.setEnabled(false);
        final MaterialDialog dialog = new MaterialDialog.Builder(ResultActitivy.this)
                .title("저장중")
                .content("이미지를 저장중 입니다.")
                .progress(true, 0)
                .show();

        mRootPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + PICFOLDER;
        File fileRoot = new File(mRootPath + "/complete");
        if (fileRoot.exists() == false) {
            if (fileRoot.mkdirs() == false) {
                Toast.makeText(getApplicationContext(), "사진을 저장할 폴더가 없습니다.", Toast.LENGTH_SHORT).show();
                finish();
                return;
            }
        }

        Calendar calendar = Calendar.getInstance();
        String FileName = String.format("SH%02d%02d%02d-%02d%02d%02d.jpg",
                calendar.get(Calendar.YEAR) % 100,
                calendar.get(Calendar.MONTH) + 1,
                calendar.get(Calendar.DAY_OF_MONTH),
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                calendar.get(Calendar.SECOND));
        final String path = mRootPath + "/complete/" + FileName;

        Log.d("completeT", path);

        Handler hd = new Handler();
        hd.postDelayed(new Runnable() {
            @Override
            public void run() {
                bitmap = viewToBitmap(layout);
                try {
                    FileOutputStream output = new FileOutputStream(path);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
                    output.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Toast.makeText(getApplicationContext(), "이미지가 저장되었습니다.", Toast.LENGTH_SHORT).show();
                share.setEnabled(true);
                addText.setEnabled(true);
                change.setEnabled(true);
                dialog.dismiss();
            }
        },500);
    }
//        layout.post(new Runnable() {
//            @Override
//            public void run() {
//
//        });


    private Bitmap viewToBitmap(View view) {
        Log.i("size", view.getWidth() + "*" + view.getHeight());
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }


}
